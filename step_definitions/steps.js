const { I } = inject();

Given('я нахожусь на странице {string}', (page) => {
  I.amOnPage(page);
});

When('я ввожу {string} в поле {string}', (value, fieldName) => {
  I.fillField({name: fieldName}, value);
});

When('я нажимаю на кнопку {string}', (button) => {
  I.click(button);
});

When('я нажимаю на {string} и выбираю {string}', (label, select) => {
  I.selectOption(label, select);
});

Then('я вижу текст {string}', (text) => {
  I.see(text);
});

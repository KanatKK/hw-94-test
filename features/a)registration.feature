# language: ru

  Функция: Регистрация пользователя
    Находясь на сайте под анонимным пользователем
    Регистрирую пользователя
    Данные должны сохраниться в базе

    Сценарий: Успешная регистрация
      Допустим я нахожусь на странице "/sign_in"
      Если я ввожу "user2" в поле "username"
      И я ввожу "Slimy" в поле "displayName"
      И я ввожу "112233" в поле "password"
      И я ввожу "https://avatarfiles.alphacoders.com/152/thumb-152197.png" в поле "imageLink"
      И я нажимаю на кнопку "regBtn"
      То я вижу текст "Slimy"